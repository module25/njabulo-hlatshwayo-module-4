import 'package:flutter/material.dart';

class LoginScreen extends StatefulWidget {
  const LoginScreen({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() => StartState();
}

class StartState extends State<LoginScreen> {
  @override
  Widget build(BuildContext context) {
    return initWidget();
  }

  initWidget() {
    return Scaffold(
      body: SingleChildScrollView(
          child: Column(
        children: [
          Container(
            height: 300,
            decoration: const BoxDecoration(
              borderRadius: const BorderRadius.only(
                  bottomLeft: const Radius.circular(90)),
              color: Color.fromARGB(255, 245, 231, 31),
              gradient: LinearGradient(
                colors: [
                  (Color.fromARGB(255, 241, 245, 31)),
                  Color.fromARGB(255, 238, 242, 30)
                ],
                begin: Alignment.topCenter,
                end: Alignment.bottomCenter,
              ),
            ),
          ),
          Container(
            margin: const EdgeInsets.only(right: 20, top: 20),
            alignment: Alignment.bottomRight,
            child: const Text(
              "Login",
              style:
                  TextStyle(fontSize: 20, color: Color.fromARGB(255, 15, 5, 5)),
            ),
          )
        ],
      )),
    );
  }
}
