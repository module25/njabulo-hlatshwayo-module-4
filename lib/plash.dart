import 'dart:async';

import 'package:flutter/material.dart';

import 'login.dart';

void main() {
  runApp(const SplashScreen());
}

class SplashScreen extends StatefulWidget {
  const SplashScreen({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() => StartState();
}

class StartState extends State<SplashScreen> {
  @override
  void initState() {
    // ignore: todo
    // TODO: implement initState
    super.initState();
    startTime();
  }

  startTime() async {
    var duration = const Duration(seconds: 4);
    // ignore: unnecessary_new
    return new Timer(duration, route);
  }

  route() {
    Navigator.pushReplacement(
        context, MaterialPageRoute(builder: (context) => LoginScreen()));
  }

  @override
  Widget build(BuildContext context) {
    return initWidget(context);
  }

  Widget initWidget(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: [
          Container(
            decoration: const BoxDecoration(
                color: Color.fromARGB(255, 231, 245, 31),
                gradient: LinearGradient(colors: [
                  (Color.fromARGB(255, 170, 245, 31)),
                  Color.fromARGB(255, 210, 242, 30)
                ], begin: Alignment.topCenter, end: Alignment.bottomCenter)),
          ),
          Center(
           
          )
        ],
      ),
    );
  }
}
